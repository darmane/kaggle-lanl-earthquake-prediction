import java.io.File
import java.util.Calendar

/*
* ##########################################################################################
*
* 2019
*
* MAIN DEL PROYECTO DE IE DE:
*   - Adrián López Carrillo
*   - Alejandro Torres Terrones
*   - José Eulalio Bernáldez Torres
*
* OBJETIVO:
*   DAR UNA SOLUCIÓN AL RETO DE KAGGLE "Earthquake Prediction"
*   EMPLEANDO LAS HERRAMIENTAS VISTAS EN LA ASIGNATURA (scala, spark, etc)
*
*
* ##########################################################################################
*/







object Main {

  def main(args: Array[String]): Unit = {
    /*
    * ###########
    *   Paths
    * ###########
    */

    val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
    val hour = Calendar.getInstance().get(Calendar.HOUR)
    val minute = Calendar.getInstance().get(Calendar.MINUTE)
    val dateTime = day + "-" + hour + "-" + minute


    // Files
    val pathToTrainingSubfile= "src/main/resources/training/training_subfile.csv"
    val pathToTraining= "src/main/resources/training/train.csv"

    val pathToTest = "src/main/resources/test/"

    // Models
    val pathToRFModel = s"src/main/resources/$dateTime/models/RandomForestModel"
    val pathToGDTModel = s"src/main/resources/$dateTime/models/GradientBootedTree"

    // Predictions
    val pathToRFPrediction = s"src/main/resources/$dateTime/submission_rf"
    val pathToGDTPrediction = s"src/main/resources/$dateTime/submission_gbt"

    /*
    * ##########################################################
    *   Split CSV file
    *
    *   WARNING:
    *     only execute this function one time to split a
    *     really big file into smaller files of n entries
    *
    *  val newPathToTraining = "src/main/resources/training/"
    *  FileIO.splitCSV(pathToTraining,newPathToTraining,1000000)
    *
    * ##########################################################
    */


    /*
    * ####################
    *   Prob EarthQuake
    * ####################
    */
    val peRF = new ProblemEarthquakeRF
    val peRF2 = new ProblemEarthquakeRF2
    val peGTB = new ProblemEarthquakeGBT

    /*
    * ################################
    *   Create DataFrame for training
    * ################################
    */
/*
    println("\n ---- RF ---- \n")
    Timer.startNow()
    peRF.getDataFromFile(pathToTraining)
    println("-- Train model")
    val modelRF = peRF.getRandomForestModel
    Timer.printNow()
    println("-- Test model")
    peRF.evaluateModelOnTestAndGetError(modelRF)
    Timer.printNow()
    println("-- Save model")
    peRF.saveModel(modelRF, pathToRFModel)
    Timer.printNow()
    println("-- Start real predictions")
    peRF.saveTestsPrediction(modelRF, pathToRFPrediction)
    Timer.printNow()
*/

    Timer.startNow()
    println("\n --- Reading Data --- \n")
    println("\n -- Reading training data --\n")
    val (trainingData, testData, featureIndexer) = peRF2.getDataFromFile(pathToTraining)
    Timer.printNow()

    Timer.startNow()
    println("\n ---- RF ---- \n")
    peRF2.fit(trainingData, testData, featureIndexer)
    peRF2.saveModel(pathToGDTModel)

    Timer.startNow()
    println("\n ---- GTB ---- \n")
    peGTB.fit(trainingData, testData, featureIndexer)
    peGTB.saveModel(pathToGDTModel)

    println("\n*** Both Models created, now using it to predict real data ***\n")

    Timer.startNow()
    println("\n -- Reading real data --\n")
    val vectorizedTest = peRF2.getVectorizedTest
    println("\n --- RF ---\n")
    peRF2.saveTestsPrediction(pathToRFPrediction, vectorizedTest)
    println("\n --- GTB ---\n")
    peGTB.saveTestsPrediction(pathToGDTPrediction, vectorizedTest)


  }

}
