
import java.io.{File, FileOutputStream, PrintWriter}

import scala.io.Source

object FileIO {

  // funcion para leer ficheros
  def readFile(pathToFile: String) =
    Source.fromFile(pathToFile).getLines()



//   ##################################################################
//       Split CSV file into a smaller files
//   ##################################################################

  def splitCSV(pathToCSVFile: String, newPath:String, size:Int) ={
     val file = readFile(pathToCSVFile)
     val header = file.take(1).next()
     val pw = new PrintWriter(new FileOutputStream(new File(newPath
             + "training_subfile.csv"),true))
     pw.write(header+"\n")
     file.take(size).foreach(x =>pw.write(x+"\n"))
     pw.close()
  }
//   ##################################################################



}
