import org.apache.hadoop.fs.DF
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.{VectorAssembler, VectorIndexer, VectorIndexerModel}
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.ml.regression.{GBTRegressionModel, GBTRegressor}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.model.GradientBoostedTreesModel
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import java.io.File

class ProblemEarthquakeGBT extends Serializable {

  // size of the DataFrame partitions
  val repartitionSize:Double = 150000.0

  // to assemble columns into a features array
  private val assembler = new VectorAssembler()
    .setInputCols(Array("kurtosis", "variance", "mean","st deviation","min","max"))
    .setOutputCol("features")

  // model
  private var model: PipelineModel = _

  // Save and load model
  def saveModel(pathToModel:String):Unit =
    model.write.overwrite().save(pathToModel)

  // Load Model
  def loadModel(pathToModel:String):Unit =
    Pipeline.load(pathToModel)

  def getDataFromFile(pathToTraining: String): (Dataset[Row], Dataset[Row], VectorIndexerModel) = {
    println("--- Start training t=0")

    println("-- Reading file ")
    val df = OpenSparkSession.spark.read
      .format("csv")
      .option("header", "true").load(pathToTraining)
    Timer.printNow()

    println("-- Transforming raw data to stats ")
    val stats = new StatisticsOP(df).getStatsDF(repartitionSize)
    Timer.printNow()

    println("-- Assembling all features into one column")
    val vectorizedDF = assembler.transform(stats)
    Timer.printNow()

    println("-- Identifying categorical features and indexing them")
    // Automatically identify categorical features, and index them.
    // Set maxCategories so features with > 4 distinct values are treated as continuous.
    val featureIndexer = new VectorIndexer()
      .setInputCol("features")
      .setOutputCol("indexedFeatures")
      .setMaxCategories(4)
      .fit(vectorizedDF)
    Timer.printNow()

    println("-- Splitting into training and test")
    // Split the data into training and test sets (30% held out for testing).
    val Array(trainingData, testData) = vectorizedDF.randomSplit(Array(0.7, 0.3))
    Timer.printNow()
    (trainingData, testData, featureIndexer)
  }

  def fit(trainingData: Dataset[Row], testData: Dataset[Row], featureIndexer: VectorIndexerModel): Unit = {
    println("-- Train model")
    // Train a GBT model.
    val gbt = new GBTRegressor()
      .setLabelCol("label")
      .setFeaturesCol("indexedFeatures")
      .setMaxIter(200)
      .setMaxBins(64)

    // Chain indexer and GBT in a Pipeline.
    val pipeline = new Pipeline()
      .setStages(Array(featureIndexer, gbt))

    // Train model. This also runs the indexer.
    model = pipeline.fit(trainingData)

    Timer.printNow()

    println("-- Making test predictions and evaluate")
    // Make predictions.
    val predictions = model.transform(testData)

    // Select example rows to display.
    predictions.select("prediction", "label", "features").show(10)

    // Select (prediction, true label) and compute test error.
    val evaluator = new RegressionEvaluator()
      .setLabelCol("label")
      .setPredictionCol("prediction")
      .setMetricName("rmse")
    val rmse = evaluator.evaluate(predictions)
    println("Root Mean Squared Error (RMSE) on test data = " + rmse)

    val gbtModel = model.stages(1).asInstanceOf[GBTRegressionModel]
    println(s"Learned regression GBT model:\n ${gbtModel.toDebugString}")

    Timer.printNow()
  }

  def getVectorizedTest: DataFrame = {
    println("-- Reading from file")
    val statsTest = TestsReader.readTestAsDF()
    Timer.printNow()

    println("-- Assembling all features into one column")
    val vectorizedTest = assembler.transform(statsTest)
    Timer.printNow()
    vectorizedTest
  }

  def saveTestsPrediction(pathToGDTPrediction: String, vectorizedTest: DataFrame): DataFrame = {
    println("-- Making predictions and evaluate")
    val predictions = model.transform(vectorizedTest)
    Timer.printNow()

    predictions.select("prediction", "fileName", "features").show(10)

    predictions.select("fileName", "prediction").write.format("csv").save(pathToGDTPrediction)

    Timer.printNow()

    predictions
  }

}
