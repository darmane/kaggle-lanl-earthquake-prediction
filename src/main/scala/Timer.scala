object Timer {
  var start: Long = System.currentTimeMillis() / 1000
  def startNow(): Unit = start = System.currentTimeMillis() / 1000
  def printNow(): Unit = {
    var s = System.currentTimeMillis / 1000 - start
    var m = s/60
    val h = s/60/60
    val elapsedTime = {
      if(h >= 1) {
        m = m - h * 60
        s"$h h, $m m"
      }
      else if(m >= 1){
        s = s - m * 60
        s"$m m, $s s"
      }
      else
        s"$s s"
    }
    println(s"Elapsed time: $elapsedTime")
  }

}
