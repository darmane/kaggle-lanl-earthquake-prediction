import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import java.io.File

import org.apache.commons.math3.stat.descriptive.moment.{Kurtosis, Mean, Variance}
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}

import scala.io.Source
import scala.collection.JavaConversions._
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

object TestsReader {
  val ss: SparkSession = SparkSession.builder()
    .appName("Test")
    .master("local[2]")
    .getOrCreate()

  def readTestAsDF(): DataFrame = {
    val file = new File("src/main/resources/test/")
    val statsSchema = StructType(Seq(
      StructField("kurtosis", DoubleType),
      StructField("variance", DoubleType),
      StructField("mean", DoubleType),
      StructField("fileName", StringType)
    ))
    ss.createDataFrame(
      file.listFiles().map(f => {
      val acousticData = Source.fromFile(f.getCanonicalFile).getLines.toArray.drop(1).map(l => l.toString.toDouble)
      val kurtosis = (new Kurtosis).evaluate(acousticData)
      val variance = (new Variance).evaluate(acousticData)
      val mean = (new Mean).evaluate(acousticData)
      Row(kurtosis, variance, mean, f.getName)
      }).toList
      , statsSchema)
  }

  def readTestAsRdd(): RDD[(String, Vector)] = {
    val file = new File("src/main/resources/test/")
    ss.sparkContext.parallelize(
      file.listFiles().map(f => {
        val acousticData = Source.fromFile(f.getCanonicalFile).getLines.toArray.drop(1).map(l => l.toString.toDouble)
        val kurtosis = (new Kurtosis).evaluate(acousticData)
        val variance = (new Variance).evaluate(acousticData)
        val mean = (new Mean).evaluate(acousticData)

        (f.getName , Vectors.dense(mean,variance,kurtosis))
      })
    )
  }
}
