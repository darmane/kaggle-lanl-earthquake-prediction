import org.apache.log4j.Level
import org.apache.log4j.Logger._
import org.apache.spark.sql.SparkSession

object OpenSparkSession {

  // spark session
  val spark = SparkSession.builder()
    .appName("EarthQuakePrediction")
    .master("local[2]")
    .getOrCreate()
  // spark context
  val sc = spark.sparkContext

  getRootLogger.setLevel(Level.ERROR)

}
