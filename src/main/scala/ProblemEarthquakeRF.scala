import org.apache.spark.ml.PipelineModel
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.RandomForest
import org.apache.spark.mllib.tree.model.RandomForestModel
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.DataFrame
import org.apache.spark.mllib.linalg.Vector


class ProblemEarthquakeRF extends Serializable {

  // SparkContext
  private val sc = OpenSparkSession.sc

  // size of the DataFrame partitions
  private val repartitionSize:Double = 150000.0

  // data for training and test
  private var trainingData: RDD[LabeledPoint] = sc.emptyRDD[LabeledPoint]
  private var testData: RDD[LabeledPoint] = sc.emptyRDD[LabeledPoint]


  def getDataFromFile(pathToFile:String):Unit = {
    println("-- Reading file ")
    // this val is the DataFrame obtained from the CSV file
    val df = OpenSparkSession.spark.read
      .format("csv")
      .option("header", "true").load(pathToFile)

    println("-- Transforming raw data to stats ")
    val rdd:RDD[LabeledPoint] = new StatisticsOP(df)
      .StatsRDD(repartitionSize)
    Timer.printNow()

    println("-- Splitting into training and test")
    // Split the data into training and test sets (30% held out for testing)
    val splits:Array[RDD[LabeledPoint]] = rdd.randomSplit(Array(0.7, 0.3))
    Timer.printNow()

    // training and test data
    trainingData = splits(0)
    testData = splits(1)
  }


   // create the random forest model
  def getRandomForestModel:RandomForestModel = {
    // Train a RandomForest model.
    val categoricalFeaturesInfo:Map[Int,Int] = Map[Int, Int]()
    val numTrees = 1000 // default = 3.   Use more in practice.
    val featureSubsetStrategy = "auto" // Let the algorithm choose.
    val impurity = "variance"
    val maxDepth = 4
    val maxBins = 32

    RandomForest.trainRegressor(trainingData, categoricalFeaturesInfo,
      numTrees, featureSubsetStrategy, impurity, maxDepth, maxBins)
  }

  // Evaluate model on test instances and compute test error
  def evaluateModelOnTestAndGetError(model:RandomForestModel):Unit = {
    val labelsAndPredictions: RDD[(Double, Double)] = testData.map { point =>
      val prediction = model.predict(point.features)
      (point.label, prediction)
    }
    val testMSE = labelsAndPredictions.map { case (v, p) => math.pow(v - p, 2) }.mean()
    println(s"Test Mean Squared Error = $testMSE")
    println(s"Learned regression forest model:\n ${model.toDebugString}")
  }

  // Save and load model
  def saveModel(model:RandomForestModel,pathToModel:String):Unit =
    model.save(OpenSparkSession.sc, pathToModel)

  // Load Model
  def loadModel(pathToModel:String):Unit =
     RandomForestModel.load(OpenSparkSession.sc, pathToModel)


  def saveTestsPrediction(model: RandomForestModel, pathToRFPrediction: String): RDD[(String, Double)] = {
    println("-- Reading from file")
    val statsTest = TestsReader.readTestAsRdd()
    Timer.printNow()

    println("-- Making predictions and evaluate")
    val predictions = statsTest.map(r => {
      val name = r._1
      val features: Vector = r._2
      (name, model.predict(features))
    })

    TestsReader.ss.createDataFrame(predictions).write.format("csv").save(pathToRFPrediction)

    predictions
  }


}
