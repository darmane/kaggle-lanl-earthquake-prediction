import org.apache.commons.math3.analysis.function.Max
import org.apache.commons.math3.stat.descriptive.moment.{Kurtosis, Mean, StandardDeviation, Variance}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.catalyst.encoders.RowEncoder
import org.apache.spark.sql.{DataFrame, Dataset, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, DoubleType, IntegerType, StructField, StructType}

import scala.collection.mutable.{ArrayBuffer, ListBuffer}


class StatisticsOP(df:DataFrame) extends Serializable {


  /*   //////////////////////////////////////////
 *      Statistic functions for Arrays
 */  //////////////////////////////////////////

  def getArrMean(arr:ArrayBuffer[Double]):Double = (new Mean).evaluate(arr.toArray)

  def getArrCount(arr:ArrayBuffer[Double]):Int = arr.length

  def getArrVariance(arr:ArrayBuffer[Double]):Double = (new Variance).evaluate(arr.toArray)

  def getArrKurtosis(arr:ArrayBuffer[Double]):Double= (new Kurtosis).evaluate(arr.toArray)



  /*   //////////////////////////////////////////
   *      Statistic Data
   */  //////////////////////////////////////////

  def getStatsDF(repartitionSize:Double = 150000.0): Dataset[Row] = {

    // Add row column to keep track of the correct order
    val train_df = df.withColumn("row", monotonically_increasing_id())

    // Make 1 partition (worker) for each 150000 points
    // then we order by row
    // and then we reduce partitions because for some reason orderBy
    // generates a lot of extra partitions
    val train_ds_part = train_df
      .repartition((train_df.count()/repartitionSize).ceil.toInt)
      .orderBy("row")
      .coalesce((train_df.count()/repartitionSize).ceil.toInt)

    // Original schema of the dataframe.
    val dataSchema = StructType(Seq(
      StructField("acoustic_data", IntegerType),
      StructField("time_to_failure", DoubleType)
    ))

    // Stats schema of a dataframe, used to map original dataframe into new one
    val statsSchema = StructType(Seq(
      StructField("kurtosis", DoubleType),
      StructField("variance", DoubleType),
      StructField("mean", DoubleType),
      StructField("St Deviation",DoubleType),
      StructField("min",DoubleType),
      StructField("max", DoubleType),
      StructField("label", DoubleType)
    ))
    val statsEncoder = RowEncoder(statsSchema)

    // For each partition (made of 150000 consecutive points) we calculate the
    // needed stats and set the ttf as the ttf of the last point
    val stats = train_ds_part
      .mapPartitions(partition => {
        val acoustic_data = new ListBuffer[Double]()
        var ttf = new ListBuffer[Double]()
        partition.foreach(row => {
          acoustic_data += row.get(0).toString.toDouble
          ttf += row.get(1).toString.toDouble
        })
        val acoustic_data_arr = acoustic_data.toArray

        val kurtosis = (new Kurtosis).evaluate(acoustic_data_arr)
        val variance = (new Variance).evaluate(acoustic_data_arr)
        val mean = (new Mean).evaluate(acoustic_data_arr)
        val stDeviation = (new StandardDeviation).evaluate(acoustic_data_arr)
        val min = acoustic_data_arr.reduceLeft(_ min _)
        val max = acoustic_data_arr.reduceLeft(_ max _)
        val features = Array(kurtosis, variance, mean,stDeviation,min,max)
        val ttfMean = (new Mean).evaluate(ttf.toArray)
        List(Row(kurtosis, variance, mean, stDeviation, min, max, ttfMean)).iterator
      }) (statsEncoder)

    // List of Stats + time_to_failure of each 150000 consecutive points
    stats
  }

  def StatsRDD(repartitionSize:Double = 150000.0): RDD[LabeledPoint] = {

    // Add row column to keep track of the correct order
    val idDF = df.withColumn("row", monotonically_increasing_id())

    // Make 1 partition (worker) for each 150000 points
    // then we order by row
    // and then we reduce partitions because for some reason orderBy
    // generates a lot of extra partitions
    val ds = idDF.repartition((idDF.count()/repartitionSize).ceil.toInt)
      .orderBy("row")
      .coalesce((idDF.count()/repartitionSize).ceil.toInt)

      ds.foreachPartition(p=>
      {
        var acSig = new ArrayBuffer[Double]
        var ttf = new ArrayBuffer[Double]
        p.foreach(row=>
        {
          acSig += row.get(0).toString.toDouble
          ttf += row.get(1).toString.toDouble
        })
        val kurt = getArrKurtosis(acSig)
        val varc = getArrVariance(acSig)
        val mean = getArrMean(acSig)
        val ttfMean = getArrMean(ttf)

        // labeled point
        val lp = LabeledPoint(ttfMean,Vectors.dense(mean,varc,kurt))

        // add LabeledPoint to list
        Data.labeledPointList += lp
      })

    OpenSparkSession.sc.makeRDD(Data.labeledPointList)
  }






}
