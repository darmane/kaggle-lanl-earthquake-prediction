# LANL Earthquake Prediction

[Can you predict upcoming laboratory earthquakes?](https://www.kaggle.com/c/LANL-Earthquake-Prediction)

## Stack

- Scala

## License

MIT License.

You can fork this project for free under the following conditions:

- Add a link to this project.
- The software is provided on an "as is" basis and without warranty of any kind. Use it at your own risk.
